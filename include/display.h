#ifndef	__DISPLAY_INFO_H__
#define	__DISPLAY_INFO_H__

#include <X11/Xlib.h>
#include <X11/Xutil.h>

typedef struct 	_di_	{
			Display *name;
			int 		depth;
			int		width;
			int 		height;
			int 		con;
} 	_display_info;

_display_info *display_alloc(_display_info *di);
_display_info *display_info(_display_info *di);

#endif
