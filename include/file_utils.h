#ifndef __FILE_UTILS_H__
#define __FILE_UTILS_H__

#define		HEADER_T		0xFF01		// Triangulos
#define		HEADER_P		0xFF02		// Poligonos
#define		HEADER_Q		0xFF04		// Quadrados
#define		HEADER_D		0xFF08		// Pontos (Dots)
#define		HEADER3D_T	0xFE01		// Triangulos	3D
#define		HEADER3D_P	0xFE02		// Poligonos	3D
#define		HEADER3D_Q	0xFE04		// Quadrados	3D
#define		HEADER3D_D	0xFE08		// Pontos (Dots)	3D
#define		HEADER_S		0x0080		// Striped
#define		HEADER_DIM	0x0040		// Arquivo contendo a dimensao

typedef	struct _hi_ 	{
		unsigned int 		mn;		// Magic Number do arquivo (id)
		int					w,h;		// Valores maximo da janela
		int					f;			// Quantidade de figuras	
		int					d;			// Dimensão dos pontos ( 1, 2 ou 3 D)
} _header_info;

int verifica_header(_header_info *hi,FILE *fp); // Verifica o Header e retorna
															// Os valores nele contido
int leitura_triangulo(int *v[3][3],int p,FILE *fp);// Le os vertices dois triangulos
int leitura_quadrado(int *v[4][3],int p,FILE *fp);	// Le os vertices dos quadrados
int leitura_pontos(int *v[1][3],int p,FILE *fp);	// Le os pontos 
/*
int leitura_poligono(int *v[][3],int p,FILE *fp);	// Le os vertices de Poligonos
*/

#endif
