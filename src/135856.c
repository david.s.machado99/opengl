#include <GL/glut.h>
#include <GL/gl.h>

void iniciar(void);
void exibir(void);

int main(int argc, char** argv)
{
  glutInit(&argc, argv);
  glutInitDisplayMode (GLUT_SINGLE | GLUT_RGB);
  glutCreateWindow ("Concatenação de Transformações");
  iniciar();
  glutDisplayFunc(exibir);
  glutMainLoop();
  return 0;
}

void iniciar(void)
{
 glClearColor(1.0, 1.0, 1.0, 0.0);
 glOrtho(-1.0,1.0,-1.0,1.0,-1.0,1.0);
}

void tracar_eixos()
{
  int i;
  glColor3f (0.0, 0.0, 0.0);
  glBegin(GL_LINES);
     glVertex2f(-1,0);
     glVertex2f(1,0);
     glVertex2f(0,-1);
     glVertex2f(0,1);
     for(i=0;i<21;i++)
     {
        glVertex2f(-0.03,-1.0+i*0.1);
        glVertex2f(0.03,-1.0+i*0.1);
        glVertex2f(-1.0+i*0.1,-0.03);
        glVertex2f(-1.0+i*0.1,0.03);
     }
  glEnd(); 
}
void desenhar_poligono(void)
{
  glBegin(GL_POLYGON);
    glVertex2f(0.0,0.0);
    glVertex2f(0.0,0.1);
    glVertex2f(0.05,0.15);
    glVertex2f(0.1,0.1);
    glVertex2f(0.1,0.0);
  glEnd();
}
void exibir()
{
     glClear(GL_COLOR_BUFFER_BIT);
     glColor3f(1.0,0.0,0.0);
     glLoadIdentity();
     desenhar_poligono();
     tracar_eixos();  
     
     glTranslatef(0.5,0.0,0.0);
     glColor3f(0.0,1.0,0.0);
     desenhar_poligono();
     
     glScalef(2.0,1.0,1.0);
     glColor3f(0.0,0.0,1.0);
     desenhar_poligono();
     
     glRotatef(45,0,0,1);
     glColor3f(1.0,1.0,0.0);
     desenhar_poligono();
     
     glFlush();
}
