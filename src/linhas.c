#include <stdio.h>
#include <stdlib.h>

#include <GL/glut.h>

void exibir(void);

void 
exibir(void)
{
  //glClearColor(1.0, 1.0, 1.0, 0.0); // especifica a cor RGBA para limpeza
                                    // do buffer
  glClear(GL_COLOR_BUFFER_BIT); //limpa o buffer com a cor
                                // epecificada em glClearColor()
  glColor3f (1.0, 0.0, 0.0);
  glBegin(GL_LINES); // inicia o desenho de uma linha
     glVertex2i(30,226); // define os v�rtices da linha
     glVertex2i(226,30);
  glEnd(); // finaliza o desenho da linha
  glColor3f (0.0, 1.0, 1.0);
  glBegin(GL_LINES); // inicia o desenho de uma linha
     glVertex2i(0,0); // define os v�rtices da linha
     glVertex2i(512,512);
  glEnd(); // finaliza o desenho da linha
  glFlush(); // descarrega o buffer
}


int 
main(int argc, char** argv)
{
  glutInit(&argc, argv);   // inicia a biblioteca GLUT
  glutInitDisplayMode (GLUT_SINGLE | GLUT_RGB); //  sistema de cor rgb e
                                                //buffer simples
  printf("\n%d\n",glutGet(GLUT_WINDOW_WIDTH));
  glutInitWindowSize(512,512);	// Define o Tamanho da Janela
  glutCreateWindow ("Desenhando uma linha");//cria e rotula
  glOrtho (0, 512, 0, 512, -1 ,1); // especifica o sistema de coordenadas
  printf("\n%d\n",glutGet(GLUT_WINDOW_WIDTH));
  glutDisplayFunc(exibir);
  glutMainLoop();
  return 0;
}
