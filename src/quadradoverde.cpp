#include <GL/glut.h>
#include <GL/gl.h>

#ifdef __WIN32__
#include<windows.h>
#endif

void iniciar(void);
void exibir(void);

int main(int argc, char** argv)
{
  glutInit(&argc, argv);
  glutInitDisplayMode (GLUT_SINGLE | GLUT_RGB);
  glutCreateWindow ("Trabalhando com Poligonos");
  iniciar();
  glutDisplayFunc(exibir);
  glutMainLoop();
  return 0;
}

void iniciar(void)
{
 glClearColor(1.0, 1.0, 1.0, 0.0);
 glOrtho(-1.0,1.0,-1.0,1.0,-1.0,1.0);
}
void exibir(void)
{
  glClear(GL_COLOR_BUFFER_BIT);
  glColor3f(0.0,1.0,0.0);
  glBegin(GL_POLYGON);
    glVertex2f(0.0,0.0);
    glVertex2f(0.0,0.3);
    glVertex2f(0.4,0.3);
    glVertex2f(0.4,0.0);
  glEnd();
  glFlush();
}

