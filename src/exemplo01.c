#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include <GL/glut.h>

/*
 * Constantes para largura e altura da janela a ser 
 * aberta pela função glutInitWindowSize(WIDTH, HEIGHT)
 *
*/
#define	WIDTH		400
#define	HEIGHT	400

int main(int argc, char *argv[]);
void Desenha(void);
void Inicializa (void);

void
Desenha(void)
{
 	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
  	glClear(GL_COLOR_BUFFER_BIT);
  	glColor3f(1.0f, 0.0f, 0.0f);	// Especifica a cor corrente: Vermelho
	/*
	 * Inicia a primitiva a ser desenha
	 *
	 * 	GL_POINTS			- Desenha pontos na tela
	 * 	GL_LINES				- A cada 2 pontos 1 reta
	 * 	GL_LINE_STRIP		- Os 2 primeiros pontos formam uma reta, a cada
	 * 							novo ponto é desenhada uma nova reta com o ponto
	 * 							anterior.
	 * 	GL_LINE_LOOP		- Os 2 primeiros pontos desenham uma reta, a cada
	 * 							novo ponto é desenhada uma nova reta com o ponto 
	 * 							anterior, e o último ponto é traçada uma reta com
	 * 							o primeiro
	 * 	GL_TRIANGLES		- A cada 3 pontos desenha um triângulo preenchido
	 * 	GL_TRIANGLE_STRIP	- Desenha triângulos ligados entre si, os 3 pri -
	 * 							meiros pontos formam o triângulo, e a cada 2 no -
	 * 							vos pontos, desenha um novo triângulo
	 * 	GL_TRIANGLE_FAN	- Desenha triângulos ligados ao primeiro ponto. 
	 * 							Os 3 primeiros pontos desenham um triângulo, o 
	 * 							próximo ponto fecha o próximo triângulo com o po-
	 * 							nto anterior e o primeiro ponto
	 * 	GL_QUADS				- Desenha um quadrado a cada 4 pontos
	 * 	GL_QUAD_STRIP		- Os primeiros 4 pontos desenham um quadrado pre-
	 * 							enchido e a cada 2 novos pontos desenha outro 
	 * 							quadrado.
	 * 	GL_POLYGON			- Desenha um poligono com N pontos. A quantidade
	 * 							minima de pontos são 3 ( triângulo )
	 *
	*/
  	glBegin(GL_LINES);
		glVertex2i(100,150);
  		glVertex2i(100,100);
  		glColor3f(0.0f, 0.0f, 1.0f); // Especifica a cor corrente: Azul
  		glVertex2i(150,100);
  		glVertex2i(150,150);               
  	glEnd();
  	glFlush();
}

void
Inicializa (void)
{   
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	gluOrtho2D (0.0f, 250.0f, 0.0f, 250.0f);
}

void
AlteraTamanhoJanela(GLsizei w, GLsizei h)
{
	if(h == 0) h = 1;
	glViewport(0, 0, w, h);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	if (w <= h) 
		gluOrtho2D (0.0f, 250.0f, 0.0f, 250.0f*h/w);
	else 
		gluOrtho2D (0.0f, 250.0f*w/h, 0.0f, 250.0f);
}

int
main(int argc, char *argv[])
{
	/*
	 * Inicializa a biblioteca GLUT. Note que são passados os 
	 * argumentos da linha de comando para esta chamada.
	*/
	glutInit(&argc, argv);
	/*
	 * Inicializa o modo de visualização
	 *
	 *		GLUT_RGBA			- Usar o modo colorido RGBA, modo padrão
	 *		GLUT_RGB				- Usar o modo colorido em RGBA. É um apelido
	 *								para GLUT_RGBA
	 *		GLUT_INDEX			- Usr o modo de cor indexada
	 * 	GLUT_SINGLE			- Utiliza apenas um buffer de visualização
	 *		GLUT_DOUBLE			- Utiliza dois buffers de visualização, pode
	 *								acelerar o processamento, enquanto um está
	 *								sendo visualizado, o outro está sendo prepa-
	 *								rado
	 *		GLUT_ACCUM			- Usar o modo com um buffer acumulativo
	 *		GLUT_ALPHA			- Utiliza o buffer de cor com o canal alpha
	 *		GLUT_DEPTH			- Liga a utilização do buffer de profundidade
	 *		GLUT_STENCIL		- Seleciona o buffer estêncil
	 *		GLUT_MULTISAMPLE	- Abilita o uso da função MULTISAMPLE
	 *		GLUT_STEREO			- Abilita o uso da função estério
	 *		GLUT_LIMINANCE		- Inicialmente utiliza tons de cinza
	 *
	*/
	glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB);
	glutInitWindowSize(WIDTH, HEIGHT);
	glutInitWindowPosition(10,10);
	glutCreateWindow("Quadrado");
	/*
	 * Inicializa um CALLBACK para função de desenhar algo na tela
	 * Essa função será chamada toda vez que o OpenGL necessitar 
	 * desenhar a tela
	 *
	 * glutDisplayFunc( CALLBACK )
	 *
	*/
	glutDisplayFunc(Desenha);
	/*
	 * Inicializa um CALLBACK para a função de modificar o tamanho
	 * da janela.  Essa função será chamada toda a vez que for ne-
	 * cessário alterar o tamanho da janela
	 *
	 * glutReshapeFunc( CALLBACK )
	*/
	glutReshapeFunc(AlteraTamanhoJanela);
	Inicializa();
	/*
	 * Loop infinito, tratando todos os eventos necessários, do OpenGL
	 * e do sistema
	*/
	glutMainLoop();
}
