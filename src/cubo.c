/*
**
** Programa para Desenhar Pontos, Triangulos ou Quadrados
** apartir de um arquivo texto contento os valores dos 
** vertices, um por linha
**
** 2013-03-30
** $Id$ 
** $Author$
** $Date$
** 
*/
#include <GL/glut.h>		// biblioteca do GLUT
#include <stdlib.h>		// bibliotecas do C
#include <stdio.h>
#include <string.h>
#ifdef __HAVE_LIB_X11__
#pragma	">>>> Running under X Window System"
#include "display_info.h"
#endif
#ifdef __WIN32__
#pragma	">>>> Running under Windows System"
#include "windows.h"
#endif


char	comando;
float	r,g,b;

int	ExitProgram;

void exibir(void);
void teclado(unsigned tecla, int x, int y);
void rodar(float angulo,char *eixo);
int my_cor(float c);
void desenha_cubo(float xp, float yp, float zp, float offset);

int main(int argc, char** argv)
{
int 		tx,ty;
int		width,height;
#ifdef __HAVE_LIB_X11__
_display_info info;
#endif

	// Inicializa as variaveis
	r=1.0;g=1.0;b=0.0;
	comando = '*';
	tx=ty=0;
	width=height=512;
	ExitProgram = 0;

#ifdef __HAVE_LIB_X11__
	display_info(&info);		// Pega as informacoes do ambiente X Window via Xlib
	printf("\
			Nome Display [ %s ]\n\
			Largura [ %d ]  Altura [ %d ]\n",info.name,info.width,info.height
			);
	tx = (info.width - width) / 2;
	ty = (info.height - height) / 2;
#endif

#ifdef __WIN32__
	tx = GetSystemMetrics(SM_CXSCREEN)/2 // para o width
	ty = GetSystemMetrics(SM_CYSCREEN)/2 // para o height
#endif

	glutInit(&argc, argv);	 							// inicia a biblioteca GLUT
	glutInitDisplayMode (GLUT_SINGLE | GLUT_RGB);//	sistema de cor rgb e
																//buffer simples
	glutInitWindowPosition(tx,ty);					// Centraliza janela

#ifdef	__FREEBSD__
	glutInitWindowSize(width,height);				// Define o Tamanho da Janela
#else
	glutInitWindowSize(512,512);				// Define o Tamanho da Janela
#endif
	glutCreateWindow (".: Cubo :.");			//cria e rotula
	glOrtho (-width, width, -height, height, -width ,width); 			// especifica o sistema de 
																//coordenadas
	glEnable(GL_DEPTH_TEST);
/*
	glDepthRange(-width,width);
	glDepthMask(GL_TRUE);
	glDepthFunc(GL_GREATER);
*/
	glutKeyboardFunc(teclado);
	glutDisplayFunc(exibir);							// Funcao para exibir quando
																// necessario redesenhar
	while ( ! ExitProgram  )
		glutMainLoopEvent();										// Aguarda eventos (infinito)
	return 0;												// Fim
}

void exibir(void)
{
int i;

	glClear(GL_COLOR_BUFFER_BIT); 		//limpa o buffer com a cor
													//especificada em glClearColor()
	glColor3f (r,g,b);						// Desenhando eixo X em vermelho
	glBegin(GL_QUAD_STRIP);
	glColor3f (r,g,b);						// Desenhando eixo X em vermelho
		glVertex3i(100,100,0);
		glVertex3i(300,100,0);
	glColor3f (b,r,g);						// Desenhando eixo X em vermelho
		glVertex3i(100,300,0);
		glVertex3i(300,300,0);
	glColor3f (g,b,r);						// Desenhando eixo X em vermelho
		glVertex3i(100,300,200);
		glVertex3i(300,300,200);
	glColor3f (1-r,1-g,1-b);						// Desenhando eixo X em vermelho
		glVertex3i(100,100,200);
		glVertex3i(300,100,200);
	glEnd();
	desenha_cubo(10.0,10.0,10.0,0.0);
	desenha_cubo(10.0,10.0,10.0,100.0);
	desenha_cubo(90.0,90.0,90.0,0.0);
	glFlush(); // descarrega o buffer
}

void desenha_cubo(float xp, float yp, float zp, float offset)
{
float px,py,pz,nx,ny,nz;

	px = xp + offset;
	py = yp + offset;
	pz = zp + offset;
	nx = -xp + offset;
	ny = -yp + offset;
	nz = -zp + offset;
//	printf("%f %f %f %f %f %f\n",px,py,pz,nz,ny,nz);
	
	glBegin(GL_QUADS);			// Face posterior
	glColor3f (r,g,b);						// Desenhando eixo X em vermelho
		glNormal3f(0.0, 0.0, 1.0);	// Normal da face
		glVertex3f(px, py, pz);
		glVertex3f(nx, py, pz);
		glVertex3f(nx, ny, pz);
		glVertex3f(px, ny, pz);
	glEnd();
	

	glBegin(GL_QUADS);			// Face frontal
	glColor3f (b,r,g);						// Desenhando eixo X em vermelho
		glNormal3f(0.0, 0.0, -1.0); 	// Normal da face
		glVertex3f(px, py, nz);
		glVertex3f(px, ny, nz);
		glVertex3f(nx, ny, nz);
		glVertex3f(nx, py, nz);
	glEnd();
	glBegin(GL_QUADS);			// Face lateral esquerda
	glColor3f (g,b,r);						// Desenhando eixo X em vermelho
		glNormal3f(-1.0, 0.0, 0.0); 	// Normal da face
		glVertex3f(nx, py, pz);
		glVertex3f(nx, py, nz);
		glVertex3f(nx, ny, nz);
		glVertex3f(nx, ny, pz);
	glEnd();
	glBegin(GL_QUADS);			// Face lateral direita
	glColor3f (1-r,1-g,1-b);						// Desenhando eixo X em vermelho
		glNormal3f(1.0, 0.0, 0.0);	// Normal da face
		glVertex3f(px, py, pz);
		glVertex3f(px, ny, pz);
		glVertex3f(px, ny, nz);
		glVertex3f(px, py, nz);
	glEnd();
	glBegin(GL_QUADS);			// Face superior
	glColor3f (1-b,1-r,1-g);						// Desenhando eixo X em vermelho
		glNormal3f(0.0, 1.0, 0.0);  	// Normal da face
		glVertex3f(nx, py, nz);
		glVertex3f(nx, py, pz);
		glVertex3f(px, py, pz);
		glVertex3f(px, py, nz);
	glEnd();
	glBegin(GL_QUADS);			// Face inferior
	glColor3f (1-g,1-b,1-r);						// Desenhando eixo X em vermelho
		glNormal3f(0.0, -1.0, 0.0); 	// Normal da face
		glVertex3f(nx, ny, nz);
		glVertex3f(px, ny, nz);
		glVertex3f(px, ny, pz);
		glVertex3f(nx, ny, pz);
	glEnd();
//	glutSwapBuffers();
}

void teclado(unsigned tecla, int x, int y){

static char 	eixo[3],modo[15];
float a;

	a = 0.0;

	if (eixo[0] == '\0')
	strcpy(eixo,"---");
	if ( tecla == 'h' || tecla == 'H' )
		printf("\nAjuda:\n\
Comandos:\n\n\
	<R>	- Mudar cor Vermelha ( Red )\n\
	<G>	- Mudar a cor Verde ( Green )\n\
	<B>	- Mudar a cor Azul ( Blue )\n\
	<X>	- Girar em torno do eixo X\n\
	<Y>	- Girar em torno do eixo Y\n\
	<Z>	- Girar em torno do eixo Z\n\
	<+>	- Altera o angulo em +1 ou a cor em +1 ( ate 255)\n\
	<->	- Altera o angulo em -1 ou a cor em -1 ( ate 0)\n\
	< >	- Sai do Comando (tecla espaco)\n\
	<0> .. <7> - Modo de profundidade\n\
	<H>	- Essa tela de ajuda\n\n");
		if ( comando == '*') {
			switch(tecla) {
				case 'r'		:
				case 'R'		:
									comando = 'R';
									break;
				case 'g'		:
				case 'G'		:
									comando = 'G';
									break;
				case 'b'		:
				case 'B'		:
									comando = 'B';
									break;
				case 'x'		:
				case 'X'		:
									comando = 'X';
									break;
				case 'y'		:
				case 'Y'		:
									comando = 'Y';
									break;
				case 'z'		:
				case 'Z'		:
									comando = 'Z';
									break;
				case 'q'		:
				case 'Q'		:
									comando = 'Q';
									ExitProgram = 1;
									break;
				case	'0'	:
								glDepthFunc(GL_NEVER);
								strcpy(modo,"GL_NEVER");
								break;
				case	'1'	:
								glDepthFunc(GL_LESS);
								strcpy(modo,"GL_LESS");
								break;
				case	'2'	:
								glDepthFunc(GL_EQUAL);
								strcpy(modo,"GL_EQUAL");
								break;
				case	'3'	:
								glDepthFunc(GL_LEQUAL);
								strcpy(modo,"GL_LEQUAL");
								break;
				case	'4'	:
								glDepthFunc(GL_GREATER);
								strcpy(modo,"GL_GREATER");
								break;
				case	'5'	:
								glDepthFunc(GL_NOTEQUAL);
								strcpy(modo,"GL_NOTEQUAL");
								break;
				case	'6'	:
								glDepthFunc(GL_GEQUAL);
								strcpy(modo,"GL_GEQUAL");
								break;
				case	'7'	:
								glDepthFunc(GL_ALWAYS);
								strcpy(modo,"GL_ALWAYS");
								break;
				default		:
									comando = '*';
									break;
			}
		}
		else
		switch ( tecla ) {
			case '+'	:
							switch ( comando ) {
								case 'X'	:
											a = 1;
											strcpy(eixo,"X");
											break;
								case 'Y'	:
											a = 1;
											strcpy(eixo,"Y");
											break;
								case 'Z'	:
											a = 1;
											strcpy(eixo,"Z");
											break;
								case 'R'	:
											r+=0.003;
											if ( r > 1.0 ) r=1.0;
											break;
								case 'G'	:
											g+=0.003;
											if ( g > 1.0 ) g=1.0;
											break;
								case 'B'	:
											b+=0.003;
											if ( b > 1.0 ) b=1.0;
											break;
							}
							break;
			case '-'	:
							switch ( comando ) {
								case 'X'	:
											a = -1;
											strcpy(eixo,"X");
											break;
								case 'Y'	:
											a = -1;
											strcpy(eixo,"Y");
											break;
								case 'Z'	:
											a = -1;
											strcpy(eixo,"Z");
											break;
								case 'R'	:
											r-=0.003;
											if ( r < 0.0 ) r=0.0;
											break;
								case 'G'	:
											g-=0.003;
											if ( g < 0.0 ) g=0.0;
											break;
								case 'B'	:
											b-=0.003;
											if ( b < 0.0 ) b=0.0;
											break;
							}
							break;
			case ' '	:
							comando='*';
		}	
	printf("Tecla %u  [ %c ] %d %d %d (rgb) Eixo %s Modo %s   \r",
		tecla,comando,my_cor(r),my_cor(g),my_cor(b),eixo,modo);
	rodar(a,eixo);
	exibir();
	fflush(stdout);
}

int my_cor(float c) {

	return ((int)(255 * c));

}

void rodar(float angulo,char *eixo){
float x,y,z;
int 	t,i;

	t = strlen(eixo);
	x=y=z=0;
	for (i=0; i<t;i++) {
		switch( *(eixo + i) ) {
			case 'x'	:
			case 'X' :
							x=1;
							break;
			case 'y'	:
			case 'Y'	:
							y=1;
							break;
			case 'z'	:
			case 'Z'	:
							z=1;
							break;
			default:
							break;
		}
	}
	glRotatef(angulo,x,y,z);
}


