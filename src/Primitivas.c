#include <GL/glut.h>
#include <stdlib.h>
#include <stdio.h>

// Função callback chamada para fazer o desenho
void Desenhar(void)
{
     glMatrixMode(GL_MODELVIEW);
     glLoadIdentity();
                   
     // Limpa a janela de visualização com a cor de fundo especificada
     glClear(GL_COLOR_BUFFER_BIT);
    
    //Pontos
     glBegin(GL_POINTS);
			glPointSize(2.0);
			glColor3f(0.5f, 0.0f, 5.0f);
			glVertex2i(10, 10);
			glVertex2i(10, 15);
			glVertex2i(10, 20);
			glVertex2i(15, 10);
			glVertex2i(15, 15);
			glVertex2i(15, 20);
	glEnd();
     
     //Linhas 2 a 2
     glBegin(GL_LINES);
			   glColor3f(0.5f, 5.0f, 5.0f);
               glVertex2i(40,40);
               glVertex2i(30,50);
               glVertex2i(40,50);
               glVertex2i(30,40);     
               glVertex2i(40,45);
               glVertex2i(30,45);           
     glEnd();
     
     //Linhas contínuas
          glBegin(GL_LINE_STRIP);
			   glColor3f(1.0f, 1.0f, 5.0f);
               glVertex2i(60,60);
               glVertex2i(60,65);
               glVertex2i(65,65);
               glVertex2i(65,60);     
               glVertex2i(70,60);
               glVertex2i(70,65);           
     glEnd();
     
     //Linhas que se completam
          glBegin(GL_LINE_LOOP);
			   glColor3f(0.0f, 5.0f, 0.0f);
               glVertex2i(80,80);
               glVertex2i(85,85);
               glVertex2i(80,90);
               glVertex2i(75,90);     
               glVertex2i(70,85);
               glVertex2i(75,80);           
     glEnd();
     
			//Polígono
          glBegin(GL_POLYGON);
               glColor3f(0.5f, 0.0f, 0.0f);
               glVertex2i(120,120);
               glVertex2i(115,125);
               glVertex2i(110,120);
               glVertex2i(110,115);     
               glVertex2i(115,110);
               glVertex2i(120,115);           
     glEnd();
		
		//Triangulos
         glBegin(GL_TRIANGLES);
			   glColor3f(0.5f, 0.0f, 0.5f);
               glVertex2i(130,130);
               glVertex2i(125,135);
               glVertex2i(120,130);
               glVertex2i(140,130);
               glVertex2i(145,135);
               glVertex2i(135,135);  
     glEnd();
     
     //Triangulos baseados nos vertices
         glBegin(GL_TRIANGLE_STRIP);
			   glColor3f(0.5f, 1.0f, 0.5f);
               glVertex2i(160,160);
               glVertex2i(160,165);
               glVertex2i(165,160);
               glVertex2i(170,165);
               glVertex2i(175,155); 
     glEnd();
     
     //Triangulo Ventilador
              glBegin(GL_TRIANGLE_FAN);
			   glColor3f(1.0f, 1.0f, 0.0f);
               glVertex2i(180,180);
               glVertex2i(170,185);
               glVertex2i(180,190);
               glVertex2i(185,185);  
     glEnd();
     

              glBegin(GL_QUAD_STRIP);
			   glColor3f(0.5f, 1.0f, 0.5f);
               glVertex2i(200,200);
               glVertex2i(200,205);
               glVertex2i(205,205);
               glVertex2i(210,210);
               glVertex2i(215,215);
               glVertex2i(215,205);  
 
     glEnd();
     

     // Executa os comandos OpenGL
     glFlush(); //faz com que qualquer comando OpenGL não executado seja executado. 
}

// Inicializa parâmetros de rendering
void Inicializar (void)
{   
    // Define a cor de fundo da janela de visualização como preta
    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
}



// Função callback chamada quando o tamanho da janela é alterado 
void AlteraTamanhoJanela(GLsizei w, GLsizei h)
{
                   // Evita a divisao por zero
                   if(h == 0) h = 1;
                           
                   // Especifica as dimensões da Viewport
                   glViewport(0, 0, w, h);

                   // Inicializa o sistema de coordenadas
                   glMatrixMode(GL_PROJECTION);
                   glLoadIdentity();

                   // Estabelece a janela de seleção (left, right, bottom, top)
                   if (w <= h) 
                           gluOrtho2D (0.0f, 250.0f, 0.0f, 250.0f*h/w);
                   else 
                           gluOrtho2D (0.0f, 250.0f*w/h, 0.0f, 250.0f);
}



// Programa Principal 
int main(int argc, char** argv)
{
	 glutInit(&argc, argv); //inicializa, sem ela não funciona.
     glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB); //avisa a GLUT que tipo de modo de exibição deve ser usado quando a janela é criada. 
     glutInitWindowSize(800,600); //especifica o tamanho em pixels da janela GLUT.
     glutInitWindowPosition(400,300); //especifica a localização inicial da janela GLUT.
     glutCreateWindow(" .: Primitivas :."); //é o comando da biblioteca GLUT que cria a janela, Este argumento corresponde a legenda para a barra de título da janela. 
     glutDisplayFunc(Desenhar); //FUNÇÃO CALLBACK, a GLUT chama a função sempre que a janela precisar ser redesenhada, É nesta função que se deve colocar as chamadas de funções OpenGL
     glutReshapeFunc(AlteraTamanhoJanela); //estabelece a função "AlteraTamanhoJanela" previamente definida como a função callback de alteração do tamanho da janela, a função "AlteraTamanhoJanela" é executada para reinicializar o sistema de coordenadas.
     Inicializar(); //Nesta função são feitas as inicializações OpenGL que devem ser executadas antes do rendering.
     glutMainLoop(); //é a função que faz com que comece a execução da “máquina de estados” e processa todas as mensagens específicas do sistema operacional
     return 0;
}
