#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#define	PI 3.1415

typedef struct _Ponto	{
	int	x, y;
} Ponto;

int main(void);

int
main(void)
{
	float t;	// Tangente
	int x,y,c;	// y = t * x + c	-> equacao da reta
	int a;		// Angulo em graus
	int xc,yc;	//	Ponto central da circunferencia
					//  (x-xc)^2 + (y+yc)^2 = r^2
	int r;
	int counter;	// contador

	Ponto	P[100];

	x = 10;
	c = 0;
	xc = yc = 0;
	r = 10;

	for ( counter =0, x = xc-r; x < (xc + r); x++)
	{
		P[counter].x =  x;
		P[counter++].y =  (sqrt(r*r - (x - xc)*(x - xc)) + yc);
		P[counter].x =  x;
		P[counter++].y = -(sqrt(r*r - (x - xc)*(x - xc)) + yc);
	}
	while ( counter )
	{
		printf("(%d,%d)\n", P[--counter].x,P[counter].y);
	}
	return 0;
}
