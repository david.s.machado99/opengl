#include <iostream>
#include <Eigen/Core>
#include <Eigen/Dense>

/**
**
**	Eigen has a main object class: Matrix
** Matrix can be Integer(i), Float(f), Double(d), Complex Float(cf)
** or Complex Double(cd). It has the 1, 2, 3, 4 or Dynamic length.
**
** Vector Object has object Matrix as its hierarchically superior object.
**
**/

using namespace std;
using namespace Eigen;

int main(void);

int
main(void)
{
	Matrix3d	M;
	Matrix3d R;

	M << 1, 2, 3, 4, 5, 6, 7, 8, 9;
	cout << "M = " << endl << M << endl;
	cout << "Matrix M has " << M.rows() << " linhas x " << M.cols() << " colunas" << endl;
	cout << "Matrix M^-1 = " << endl << M.inverse() << endl;
	R = M.transpose() ;
	cout << "R = " << endl << R << endl;
	return 0;
}
