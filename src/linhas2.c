#include <X11/Xlib.h>
#include <err.h>
#include <stdlib.h>
#include <stdio.h>
#include <GL/gl.h>
#include <GL/glut.h>

#include "displayinfo.h"

#define	WIDTH	640
#define	HEIGHT 480

int main(int argc, char *argv[]);
void desenha(void);
void init(int argc, char *argv[], int ww, int wh);
void idle(void);


void
idle(void)
{
	desenha();
}


void
init(int argc, char *argv[], int ww, int wh)
{
int	px, py;
int	width, height;
displayinfo	di;

	width = 10;
	height = 10;

	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB);
	getDisplayInfo(&di);
	px = ( di.width - ww ) / 2;
	py = ( di.height - wh ) / 2;
	glutInitWindowPosition(px,py);
	glutInitWindowSize(ww,wh);
	glutCreateWindow("-< Primitivas: Pontos >-");
	gluOrtho2D(-width, width, -height, height);	// NOTE: This GLU function 
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
	glClear(GL_COLOR_BUFFER_BIT);
}


void
desenha(void)
{
/*
 * Esta função demonstra como desenhar pontos na tela
*/
	glClear(GL_COLOR_BUFFER_BIT);		// Limpa a tela com a cor configurada
	glBegin(GL_LINES);					// Configura a maquina de estado para 
												// desenhar linhas
		glVertex2i( -2, -2);
		glVertex2i(  2,  2);
		glVertex2i( -2,  2);
		glVertex2i(  2, -2);
		glVertex2i( -2,  0);
		glVertex2i(  2,  0);
		glVertex2i(  0, -2);
		glVertex2i(  0,  2);
	glEnd();									// Termina o desenho
	glFlush();
	glutSwapBuffers();
}

int
main (int argc, char *argv[])
{
	init(argc, argv, 640, 480);
	glutIdleFunc(idle);
	glutDisplayFunc(idle);
	glutMainLoop();
	return 0;
}


