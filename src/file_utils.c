/*
** Arquivo de funcoes utilitarias para ler/gravar 
** arquivos de aplicacao em OpenGL
**
** $Author$ Nilton Jose Rizzo
** $ID: 0.2 $
** Inicio: 20130320
** Alteracao: 20140612
**		Acrescentado na estrutura header o atributo dimensao
**		Corrigindo a leitura em 3D das funcoes
**		Acrescentado a leitura da dimensao na funcao verifica_header
**
*/

#include <stdio.h>
#include <stdlib.h>
#include "file_utils.h"
/*
** verifica_header
**
** Verifica se o arquivo em questao e do tipo
** da aplicacao, verificando o Magic Number
**
** Argumentos:
**		_header_info_ *hi
**				Ponteiro para a estrutura de header do arquivo
**		FILE *fp
**				Ponteiro para o arquivo aberto
**
**	Retorna
**		-1	Se ocorrer falhas ou o Magic Number nao corresponder
**    Numero de figuras no arquivo
**
*/
int 
verifica_header(_header_info *hi, FILE *fp) 
{
int  aux;
	rewind(fp);								// Reposiciona o arquivo no inicio
	fscanf(fp,"%X",&hi->mn);				// Le o Magic Number
	aux = hi->mn & (~HEADER_S);
	switch( aux ) {
			case 	HEADER_T	:
			case 	HEADER_Q	:
			case	HEADER_P	:
			case	HEADER_D	:
			case 	HEADER3D_T	:
			case 	HEADER3D_Q	:
			case	HEADER3D_P	:
			case	HEADER3D_D	:
					fscanf(fp,"%d %d",&hi->w, &hi->h);	// Le o tamanho da janela
					fscanf(fp,"%d",&hi->f);				// Le a quantidade de figuras
					if (aux & HEADER_DIM)
						fscanf(fp,"%dc",&hi->d);
					break;
			default			:
					hi->f = -1;
					break;
	}
	return (hi->f);							// Retorna a quantiadade de figuras
}

/*
** leitura_triangulo
**
**		Le os vertices dos triangulos no arquivo
**
**	Argumentos
**		int *v[3][3]
**					Vetor que contera os vertices dos triangulos
**
**		FILE *fp
**					Ponteiro para o arquivo aberto
**
**	Retorno
**		Retorna a quantidade de triangulos lidos
**
*/
int 
leitura_triangulo(int *v[3][3],int p,FILE *fp)
{
int i;
_header_info	hi;

		if (verifica_header(&hi,fp) && 
			!(((hi.mn & HEADER_T) == HEADER_T) ||
			  ((hi.mn & HEADER3D_T) == HEADER3D_T)) ) {
			printf("Magic Number diferente do desejado [ %0X ]\n",hi.mn);
			return (-1);
		}
		for (i=0;!feof(fp);i++) {
			if ( p == 2 )		// Le em 2D ou 3D
				fscanf(fp,"%d %d %d %d %d %d",
					&v[0][0][i],&v[0][1][i],
					&v[1][0][i],&v[1][1][i],
					&v[2][0][i],&v[2][1][i]);
			else
				fscanf(fp,"%d %d %d %d %d %d %d %d %d",
					&v[0][0][i],&v[0][1][i],&v[0][2][i],
					&v[1][0][i],&v[1][1][i],&v[0][2][i],
					&v[2][0][i],&v[2][1][i],&v[0][2][i]);
				
		}
		return (i);
}

int 
leitura_quadrado(int *v[4][3],int p,FILE *fp)
{
int i;
_header_info	hi;

		if (verifica_header(&hi,fp) && 
			!(((hi.mn & HEADER_Q) == HEADER_Q) ||
			((hi.mn & HEADER3D_Q) == HEADER_Q)) ) {
			printf("Magic Number diferente do desejado [ %0X ]\n",hi.mn);
			return (-1);
		}
		for (i=0;!feof(fp);i++) {
			if ( p == 2 )		// Le em 2D ou 3D
				fscanf(fp,"%d %d %d %d %d %d %d %d",
					&v[0][0][i],&v[0][1][i],
					&v[1][0][i],&v[1][1][i],
					&v[2][0][i],&v[2][1][i],
					&v[3][0][i],&v[3][1][i]);
			else
				fscanf(fp,"%d %d %d %d %d %d %d %d %d %d %d %d",
					&v[0][0][i],&v[0][1][i],&v[0][2][i],
					&v[1][0][i],&v[1][1][i],&v[0][2][i],
					&v[2][0][i],&v[2][1][i],&v[0][2][i],
					&v[3][0][i],&v[3][1][i],&v[0][2][i]);
		}
		return (i);
}

int 
leitura_pontos(int *v[1][3],int p,FILE *fp)
{
int i;
_header_info	hi;

		if (verifica_header(&hi,fp) && 
			!(((hi.mn & HEADER_D) == HEADER_D) || 
			((hi.mn & HEADER3D_D) == HEADER3D_D)) ) {
			printf("Magic Number diferendo do desejado [ %0X ]\n",hi.mn);
			return (-1);
		}
		for (i=0;!feof(fp);i++) {
			if ( p == 2 )
				fscanf(fp,"%d %d",
					&v[0][0][i],&v[0][1][i]);
			else
				fscanf(fp,"%d %d %d",
					&v[0][0][i],&v[0][1][i],&v[0][2][i]);
		}
		return (i);
}
