#include <stdio.h>
#include <stdlib.h>
// Este código está baseado no Simple.c, exemplo 
// disponível no livro "OpenGL SuperBible", 
// 2nd Edition, de Richard S. e Wright Jr.

// Biblioteca FreeGlut
#include <GL/glut.h>

int main(int argc, char *argv[]);
void Desenha (void);
void Inicializa (void);

// Função callback chamada para fazer o desenho
void 
Desenha
(void)
{
 //Limpa a janela de visualização com a cor de fundo especificada 
 glClear(GL_COLOR_BUFFER_BIT);

 //Executa os comandos OpenGL 
 glFlush();
	printf("Desenhando ....\n");
}

// Inicializa parâmetros de rendering
void 
Inicializa 
(void)
{   
    // Define a cor de fundo da janela de visualização como preta
    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
}

// Programa Principal 
int 
main
(int argc, char *argv[])
{
 glutInit(&argc, argv );	// Inicializa a biblioteca GLUT
 glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB); // Inicializa a tela
 glutCreateWindow("Primeiro Programa"); // Cria a Janela com o título 
 glutDisplayFunc(Desenha);	// Função de desenho
 Inicializa();	// Função do usuário
 glutMainLoop(); // Mantem o programa funcionando em loop
}
