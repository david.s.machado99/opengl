#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#define PI		3.1415

int main(void);

int
main(void)
{
unsigned int 	n,i,l;			// Numero de lados do poligono
double			x0, y0, z0,	// Coordenadas do ponto central
					xi, yi, zi,	// Coordenadas do ponto inicial
					*x, *y, *z;	// Vetor de coordenadas 
/*
Caso utilize a funcao glVertex3dv
double			*p[3];
*/
double			teta, aux, 
					cteta, steta, 
					sa, ca, r;

	n = 0;
	while ( n < 3 )
	{
		printf("Quantos lados o poligono tera? ");
		scanf("%u", &n);
	}

	printf("Digite o comprimento da aresta (lado): ");
	scanf("%u", &l);

	printf("Digite o ponto central (X0, Y0, Z0): ");
	scanf("%lf %lf %lf", &x0, &y0, &z0);

	printf("Digite o ponto inicial (Xi, Yi, Zi): ");
	scanf("%lf %lf %lf", &xi, &yi, &zi);

/*
	Alocação de um vetor de pontos 3D
	for (i =0; i < 3 ; i ++){
		p[i] = (double *)(malloc(sizeof(double) * n));
		if	 ( p[i] == (double *)(NULL) )
		{
			printf ("Erro de allocacao de memoria (X)\n");
			exit (-1);
		}
	}
*/	

	x = (double *)(malloc(sizeof(double) * n));
	if ( x == (double *)(NULL) )
	{
		printf ("Erro de allocacao de memoria (X)\n");
		exit (-1);
	}

	y = (double *)(malloc(sizeof(double) * n));
	if ( y == (double *)(NULL) )
	{
		printf ("Erro de allocacao de memoria (Y)\n");
		exit (-1);
	}

	z = (double *)(malloc(sizeof(double) * n));
	if ( z == (double *)(NULL) )
	{
		printf ("Erro de allocacao de memoria (Z)\n");
		exit (-1);
	}

	teta = 2 * PI /n;
	cteta = cos(teta);
	steta = sin(teta);
	r = l / ( 2 * sin(PI/n));
	sa =sin ((yi - y0) / r);
	ca =cos ((xi - x0) / r);
	x[0] = xi; y[0] = y0; z[0] = z0;
	for ( i = 1; i < n+1; i++)
	{
		x[i] = x0 + r * ca;
		y[i] = y0 + r * sa;
		z[i] = z0;
		aux = sa * cteta + ca * steta;
		ca = ca * cteta - sa * steta;
		sa = aux;
		printf ("Pontos: (%lf, %lf, %lf)\n", x[i], y[i], z[i]);
	}

	free(x);
	free(y);
	free(z);
	return 0;

}
