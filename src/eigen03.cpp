#include <iostream>
#include <Eigen/Core>

/**
**
**	Eigen has a main object class: Matrix
** Matrix can be Integer(i), Float(f), Double(d), Complex Float(cf)
** or Complex Double(cd). It has the 1, 2, 3, 4 or Dynamic length.
**
** Vector Object has object Matrix as its hierarchically superior object.
**
**/

using namespace std;
using namespace Eigen;

int main(void);

int
main(void)
{
	Matrix2d	M;
	Vector2d	V;
	Matrix2d R;

	M << 1, 2, 3, 4;
	V << 1, 2;
	cout << "M = " << endl << M << endl;
	cout << "Matrix M has " << M.rows() << " linhas x " << M.cols() << " colunas" << endl;
	cout << "V = " << endl << V << endl;
	cout << "Vector V has " << V.rows() << " linhas x " << V.cols() << " colunas" << endl;
	/*
	R = V * M ;
	cout << "R = " << endl << R << endl;
	cout << "Vector R has " << R.rows() << " linhas x " << R.cols() << " colunas" << endl;
	*/
	return 0;
}
