/**
*** Template para criacao de programas
*** utilizando OpenGL
***
***/

#include <GL/glut.h>		// biblioteca do GLUT
#include <stdlib.h>		// bibliotecas do C
#include <stdio.h>
#include <string.h>

int main(int argc, char* argv[]) {
int 	tx,ty;
int		Width,height;
int		wr,ww,wh,qw,qh,rr;

	// Inicializa as variaveis
	tx=ty=0;
	width=height=100;
	qw=100;qh=50;
	ww=wh=100;
	wr = ww / wh;
	qr = qw / qh;
	rr = wr / qr;
	tx = (800 - width) / 2;
	ty = (600 - height) / 2;

	glutInit(&argc, argv);	 					// inicia a biblioteca GLUT
	glutInitDisplayMode (GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);//	sistema de cor rgb e
														//buffer simples
	glutInitWindowPosition(tx,ty);			// Centraliza janela

	glutInitWindowSize(width,height);		// Define o Tamanho da Janela
	glutCreateWindow (".: Objetos :.");		//cria e rotula
	glEnable(GL_DEPTH_TEST);
	//  Funcoes de CallBacks
	glutMouseFunc(mouse);
	glutReshapeFunc(redesenha);
	glutMotionFunc(mouse_move);
	glutIdleFunc(idle);
	// Fim CallBacks
	glLoadIdentity();
	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glOrtho (0,ww,0,wh,-10,10); 			// especifica o sistema de 
	glViewport(qx,qy,qw,qh);
																//coordenadas
	glutMainLoop();										// Aguarda eventos (infinito)
	return 0;												// Fim
}
