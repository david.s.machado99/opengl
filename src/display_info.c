#include "display.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#ifdef __HAVE_LIB_X11__
#include "display_info.h"
#endif


_display_info *display_alloc(_display_info *di){

		if ((di = (_display_info *)(malloc(sizeof(_display_info))))==(_display_info *)(NULL)) {
			printf("Erro de Allocacao: display_alloc:\n");
			exit(-1);
		}
		return (di);
}

_display_info *display_info(_display_info *di){

	Display *display_name;
	int depth,screen,len;


	/*Opening display and setting defaults*/
	display_name 	= XOpenDisplay(NULL);
	screen 			= DefaultScreen(display_name);
	len 				= strlen(XDisplayName((char*)display_name));
	di->name 		= (Display *)(calloc(len+1,sizeof(char)));
	if (di->name != (Display *)(NULL))
		strncpy(di->name,XDisplayName((char*)display_name),len);
	di->width 		= DisplayWidth(display_name,screen),
	di->height 		= DisplayHeight(display_name,screen);
	di->depth 		= DefaultDepth(display_name,screen);
	di->con 			= ConnectionNumber(display_name);

	/*Closing the display*/
	XCloseDisplay(display_name);

	return ( di );
}
