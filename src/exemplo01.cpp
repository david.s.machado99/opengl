#include <GL/gl.h>
#include <GL/glut.h>
#include <GL/freeglut_std.h>
#include <GL/freeglut_ext.h>
#include <iostream>
#include <cmath>
#include <Eigen/Core>
#include <Eigen/Dense>

#define 	WIDTH		600
#define	HEIGHT	600

using namespace Eigen;
using namespace std;

int main(int argc, char *argv[]);					// Funcao principal
void desenha(void);										// Callback para desenhar na tela
void init(void);											// Inicializa os procedimentos
void AlteraTamanhoJanela(GLint w, GLint h);		// Callback para alterar o tamanho da janela
void Keybiard(unsigned char key, int x, int y);	// Callback para gerenviar o teclado
void Idle(void);											// Callback para gerenciar o tempo parado
void help(void);											// Funcao para mostrar a tela de ajuda


Vector3d	P[3], O[3];
Matrix3d	R,T;
int angulo;
int frames;
int getout;
int verbose;
int rotateorigin;

void
Idle(void)
{
	desenha();
}

void
help(void)
{
		  cout << "\n\
\th\t- This help message\n\
\tq\t- Quit program\n\
\tr\t- Rotare on origin or on point P\n\
\tv\t- Verbose mode, show more mesages\n";
}
void
Keyboard(unsigned char key, int x, int y)
{
		  cout << "Frame: " << frames << " Tecla: " << key << " Position: " << x << ", " << y << '\r';
		  cout << flush ;
		  if ( key == 'q' || key == 'Q' )	// Exit program
					 getout = 1;
		  if ( key == 'v' || key == 'V' )	// Toggle verbose mode
					 verbose^=1;
		  if ( key == 'r' || key == 'R' )	// Toggle rotate mode
					 rotateorigin^=1;
		  if ( key == 'h' || key == 'H' || key == '?' )	// Show help screen
					 help();
}


void
AlteraTamanhoJanela(GLint w, GLint h)
{
	h = ( h == 0 ) ? 1 : h;
	glViewport(0,0,w,h);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	if ( w <= h )
		gluOrtho2D(0.0f, 250.0f, 0.0f, 250.0f*h/w);
	else
		gluOrtho2D(0.0f, 250.0f*h/w, 0.0f, 250.0f);
}

void
init(void)
{
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	gluOrtho2D(0.0f, 2.0f, 0.0f, 2.0f);
}

void
desenha(void)
{
	Matrix3d	Result;
	float a;
	unsigned int count;
	float	Tx, Ty;
	IOFormat CommaInitFmt(StreamPrecision, DontAlignCols, ", ","; ",""," ", "");

	Tx = P[0][0];
	Ty = P[0][1];

	if ( angulo > 360 )
		angulo = 0;
	a = angulo * 3.1415;

	R << cos(a) , -sin(a), 0, sin(a), cos(a), 0, 0, 0, 1;
	T << 1 , 0, -Tx, 0, 1, -Ty, 0, 0, 1;
	Result = T;
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	
	if ( verbose )
		cout << "Frame: " << frames << endl;
	glBegin(GL_TRIANGLES);
		for(count=0;count<3;count++)
			glVertex2d(O[count][0], O[count][1]);
	glEnd();
	glBegin(GL_TRIANGLES);
		for(count=0;count<3;count++)
		{
			if ( rotateorigin )
				P[count] = R * Result * P[count];
			else
				P[count] = Result * R * P[count];
			T << 1 , 0,  Tx, 0, 1,  Ty, 0, 0, 1;
			P[count]= T * P[count];
			glVertex2d(P[count][0], P[count][1]);
		}
	glEnd();
	angulo+=10;
	glFlush();
	glutSwapBuffers();
	frames++;
}

int
main(int argc, char *argv[])
{
	P[0] << 100, 100, 1;
	P[1] <<  50, 150, 1;
	P[2] << 120, 160, 1;
	O[0] = P[0];
	O[1] = P[1];
	O[2] = P[2];
	frames=0;
	getout=0;
	rotateorigin=1;
	cout << "P inicial: " << P[0] << endl;
	angulo=0;
	glutInit(&argc,argv);
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB);
	glutInitWindowSize(WIDTH,HEIGHT);
	glutInitWindowPosition(0,0);
	glutCreateWindow("Triangulos em movimento");
	init();
	glutDisplayFunc(desenha);
	glutReshapeFunc(AlteraTamanhoJanela);
	glutKeyboardFunc(Keyboard);
	glutIdleFunc(Idle);
	while ( !getout )
	{
		glutMainLoopEvent();
		desenha();
	}
	//glutMainLoop();
	return 0;
}
