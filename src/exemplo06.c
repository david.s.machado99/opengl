#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include <GL/glut.h>

#define	WIDTH	400
#define	HEIGHT	400

int main(int argc, char *argv[]);
void Desenha(void);
void Inicializa (void);

void
Desenha(void)
{
 	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
  	glClear(GL_COLOR_BUFFER_BIT);
  	glColor3f(0.0f, 1.0f, 0.0f);	// Especifica a cor corrente: Vermelho
  	glBegin(GL_TRIANGLE_FAN);
		glVertex2i(100,100);
  		glVertex2i(50,150);
  		glVertex2i(120,160);
  		glVertex2i(140,130);
  		glVertex2i(150,105);
  		glVertex2i(120,50);
  		glVertex2i(70,200);
  	glEnd();
  	glFlush();
}

void
Inicializa (void)
{   
    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
}

void
AlteraTamanhoJanela(GLsizei w, GLsizei h)
{
	if(h == 0) h = 1;
	glViewport(0, 0, w, h);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	if (w <= h) 
		gluOrtho2D (0.0f, 250.0f, 0.0f, 250.0f*h/w);
	else 
		gluOrtho2D (0.0f, 250.0f*w/h, 0.0f, 250.0f);
}

int
main(int argc, char *argv[])
{
	  glutInit(&argc, argv);	 					// inicia a biblioteca GLUT
     glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB);
     glutInitWindowSize(WIDTH, HEIGHT);
     glutInitWindowPosition(10,10);
     glutCreateWindow("Quadrado");
     glutDisplayFunc(Desenha);
     glutReshapeFunc(AlteraTamanhoJanela);
     Inicializa();
     glutMainLoop();
}
